#!/usr/bin/env ruby
#
# Copyright (C) 2017  Denver Gingerich <denver@ossguy.com>
#
# This file is part of jmp-fwdcalls.
#
# jmp-fwdcalls is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# jmp-fwdcalls is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with jmp-fwdcalls.  If not, see <http://www.gnu.org/licenses/>.

$stdout.sync = true

puts "JMP Forward Calls - for use with Catapult\n"\
        "==>> last commit of this version is " + `git rev-parse HEAD` + "\n"
puts "Copyright (C) 2017 Denver Gingerich.  jmp-fwdcalls is licensed under\n" +
	"AGPLv3+.  You can download the Complete Corresponding Source code\n" +
	"from https://gitlab.com/ossguy/jmp-register .\n\n"

t = Time.now
puts "LOG %d.%09d: starting...\n\n" % [t.to_i, t.nsec]

require 'goliath'
require 'json'
require 'net/http'
require 'redis/connection/hiredis'
require 'uri'

if ARGV.size != 4 then
	puts "Usage: jmp-fwdcalls.rb <http_listen_port> <fwdcalls_url> " +
		"<redis_hostname> <redis_port>"
	exit 0
end

class WebhookHandler < Goliath::API
	def response(env)
		params = JSON.parse Rack::Request.new(env).body.read

		t = Time.now
		if params.key?('cause')
			puts "LOG %d.%09d: %s - %s - %s\n" % [t.to_i, t.nsec,
				params['callId'], params['eventType'],
				params['cause'] ]
		else
			puts "LOG %d.%09d: %s - %s\n" % [t.to_i, t.nsec,
				params['callId'], params['eventType'] ]
		end

		conn = Hiredis::Connection.new
		begin
			conn.connect(ARGV[2], ARGV[3].to_i)
		rescue => e
			puts 'ERROR: Redis connection failed: ' + e.inspect
			return [500, {'Content-Type' => 'text/plain'},
				e.inspect]
		end

		if params.key?('tag')
			# verification call started or done; get needed details

			cred_key = 'catapult_cred-' + params['tag']
			conn.write ["LRANGE", cred_key, 0, 3]
			# we don't actually use users_num, but easier to read
			user_id, api_token, api_secret, users_num = conn.read
		end

		uri = URI.parse('https://api.catapult.inetwork.com')
		http = Net::HTTP.new(uri.host, uri.port)
		http.use_ssl = true

		case params['eventType']
		when 'incomingcall'
			# someone is calling the user's JMP number so call user

			jid_key = 'catapult_jid-' + params['to']
			fwd_key = 'catapult_fwd-' + params['to']
			conn.write ["MGET", jid_key, fwd_key]
			jid, fwdphone = conn.read

			cred_key = 'catapult_cred-' + jid
			conn.write ["LRANGE", cred_key, 0, 3]
			# we don't actually use users_num, but easier to read
			user_id, api_token, api_secret, users_num = conn.read

			if fwdphone.nil? or fwdphone.empty?
				# pickup the call so we can talk to it
				request = Net::HTTP::Post.new('/v1/users/' +
					user_id + '/calls/' + params['callId'])
				request.basic_auth api_token, api_secret
				request.add_field(
					'Content-Type', 'application/json')
				request.body = JSON.dump({'state' => 'active'})
				response = http.request(request)
				# TODO: confirm successful response; error if no

				# say the pre-recorded message
				request = Net::HTTP::Post.new('/v1/users/' +
					user_id + '/calls/' + params['callId'] +
					'/audio')
				request.basic_auth api_token, api_secret
				request.add_field(
					'Content-Type', 'application/json')
				request.body = JSON.dump({
				'sentence' =>
					'This phone number does not receive ' +
					'voice calls; please send a text ' +
					'message instead.  This phone number ' +
					'does not receive voice calls; ' +
					'please send a text message instead.',
				'tag' => jid
        	        	})
				response = http.request(request)
				# TODO: confirm successful response; error if no

				return [200, {}, "OK"]
			end


			# create a bridge and add the incoming call to it
			request = Net::HTTP::Post.new('/v1/users/' + user_id +
				'/bridges')
			request.basic_auth api_token, api_secret
			request.add_field('Content-Type', 'application/json')
			request.body = JSON.dump({
				'bridgeAudio'	=> 'true',
				'callIds'	=> [params['callId'] ]
       	        	})
			response = http.request(request)
			# TODO: confirm successful response; error if not

			puts 'bAPI response: ' + response.to_s + ' with code ' +
				response.code + ', body "' + response.body + '"'
			if response.code != '201'
				puts 'from ' + params['from'] + ' to ' +
					params['to'] + ' fwding to ' + fwdphone
			end

			bridgeId = File.basename(response['location'])


			# convert fwdphone into the format Catapult accepts
			if fwdphone.start_with?('tel:')
				fwdphone = fwdphone[4..-1]
			end


			# call the user's fwdphone, adding it to the bridge
			http = Net::HTTP.new(uri.host, uri.port)
			http.use_ssl = true

			request = Net::HTTP::Post.new('/v1/users/' + user_id +
				'/calls')
			request.basic_auth api_token, api_secret
			request.add_field('Content-Type', 'application/json')
			request.body = JSON.dump({
				# TODO: do from => params['to'] if user wants it
				'from'		=> params['from'],
				'to'		=> fwdphone,
				'bridgeId'	=> bridgeId,
				# TODO: add recordingEnabled if user wants it
				'callbackUrl'	=> ARGV[1]
       	        	})
			response = http.request(request)
			# TODO: confirm successful response; error if not

			puts 'cAPI response: ' + response.to_s + ' with code ' +
				response.code + ', body "' + response.body + '"'
			if response.code != '201'
				puts 'from ' + params['from'] + ' to ' +
					params['to'] + ' fwding to ' + fwdphone
			end

			locCallId = File.basename(response['location'])


			# JID stays around for two days, call mappings for 1 day
			cal_key = 'bri-call_jid-' + locCallId
			conn.write ["SETEX", cal_key, 172800, jid]
			if conn.read != 1
				# TODO: error
			end

			# SET in this order to reduce chance of call hangup race
			# first we link the incoming call to the forwarding call
			brf_key = 'bri-to_fwdnum-' + params['callId']
			conn.write ["SETEX", brf_key, 86400, locCallId]
			if conn.read != 1
				# TODO: error
			end
			brj_key = 'bri-to_jmpnum-' + locCallId
			conn.write ["SETEX", brj_key, 86400, params['callId'] ]
			if conn.read != 1
				# TODO: error
			end
		when 'answer'
			if not params.key?('tag')
				# user picked up incoming call to their JMP num

				cal_key = 'bri-call_jid-' + params['callId']
				brj_key = 'bri-to_jmpnum-' + params['callId']
				conn.write ["MGET", cal_key, brj_key]
				jid, to_jmpnum_callid = conn.read

				if jid.nil?
					# do nothing when caller to JMP answers
					conn.disconnect
					return [200, {}, "OK"]
				end

				cred_key = 'catapult_cred-' + jid
				conn.write ["LRANGE", cred_key, 0, 3]
				# we don't actually use unum, but easier to read
				user_id, api_token, api_secret, unum = conn.read

				# pickup the other side of the call so connected
				request = Net::HTTP::Post.new('/v1/users/' +
					user_id + '/calls/' + to_jmpnum_callid)
				request.basic_auth api_token, api_secret
				request.add_field(
					'Content-Type', 'application/json')
				request.body = JSON.dump({'state' => 'active'})
				response = http.request(request)
				# TODO: confirm successful response; error if no

			else
				# user picked up verification call; say the code

				pcode_key = 'reg-pcode-' + params['to']
				conn.write ['GET', pcode_key]
				pcode = conn.read

				# insert break between each char of 8-char pcode
				pce = pcode.gsub(/^(.)(.)(.)(.)(.)(.)(.)(.)$/,
					';\1;\2;\3;\4;\5;\6;\7;\8;')

				request = Net::HTTP::Post.new('/v1/users/' +
					user_id + '/calls/' + params['callId'] +
					'/audio')
				request.basic_auth api_token, api_secret
				request.add_field(
					'Content-Type', 'application/json')
				request.body = JSON.dump({
				'sentence' =>
					'Your JMP verification code is ' + pce +
					'Your JMP verification code is ' + pce +
					'Your JMP verification code is ' + pce +
					'Your JMP verification code is ' + pce +
					'Your JMP verification code is ' + pce,
				'tag' => params['tag']
        	        	})
				response = http.request(request)
				# TODO: confirm successful response; error if no
			end
		when 'speak'
			if params['status'] == 'done'
				# hangup call when code or "no voice" audio done
				request = Net::HTTP::Post.new('/v1/users/' +
					user_id + '/calls/' + params['callId'])
				request.basic_auth api_token, api_secret
				request.add_field(
					'Content-Type', 'application/json')
				request.body =
					JSON.dump({ 'state' => 'completed' })
				response = http.request(request)
				# TODO: confirm successful response; error if no
			end
			# on other status (ie. starting or similar) do nothing

		when 'hangup'
			# user hungup verification code, "no voice", or fwd
			#  call, or caller to JMP hung up

			state = 'completed'
			if params.key?('cause') and
				params['cause'] == 'USER_BUSY'

				state = 'rejected'
			end

			brf_key = 'bri-to_fwdnum-' + params['callId']
			brj_key = 'bri-to_jmpnum-' + params['callId']
			conn.write ["MGET", brf_key, brj_key]
			to_fwdnum_callid, to_jmpnum_callid = conn.read

			callid_to_hangup = nil
			if not to_fwdnum_callid.nil?
				# call that was hungup is call to the JMP number
				cal_key = 'bri-call_jid-' + to_fwdnum_callid
				callid_to_hangup = to_fwdnum_callid
			elsif not to_jmpnum_callid.nil?
				# call that was hungup is call to the fwd number
				cal_key = 'bri-call_jid-' + params['callId']
				callid_to_hangup = to_jmpnum_callid

			# else: do nothing - user hungup code or "no voice" call
			# TODO: slight chance of incomingcall race - deal with?
			#  could check tag to be sure (if has no tag, it's race)
			end

			if not callid_to_hangup.nil?
				conn.write ["GET", cal_key]
				jid = conn.read

				cred_key = 'catapult_cred-' + jid
				conn.write ["LRANGE", cred_key, 0, 3]
				# we don't actually use unum, but easier to read
				user_id, api_token, api_secret, unum = conn.read

				# hangup the other side, since this side hungup
				request = Net::HTTP::Post.new('/v1/users/' +
					user_id + '/calls/' + callid_to_hangup)
				request.basic_auth api_token, api_secret
				request.add_field(
					'Content-Type', 'application/json')
				request.body = JSON.dump({'state' => state})
				response = http.request(request)
				# TODO: confirm successful response; error if no
			end
		end
		# TODO: do something with response, ie. error on non-200 result

		conn.disconnect

		[200, {}, "OK"]
	end
end

EM.run do
	server = Goliath::Server.new('0.0.0.0', ARGV[0].to_i)
	server.api = WebhookHandler.new
	server.app = Goliath::Rack::Builder.build(server.api.class, server.api)
	server.logger = Log4r::Logger.new('goliath')
	server.logger.add(Log4r::StdoutOutputter.new('console'))
	server.logger.level = Log4r::INFO
	server.start
end
